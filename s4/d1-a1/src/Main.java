public class Main {
    public static void main(String[] args) {


        Car car1 = new Car();
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);
//
//        car1.make = "Veyron";
//        car1.brand = "Bugatti";
//        car1.price = 2000000;
//
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);
        //Each instance of a class should be independet from one another especially their properties. However,

        Car car2 = new Car();
//        car2.make = "Tamaraw Fx";
//        car2.brand = "Toyota";
//        car2.price = 450000;
//        System.out.println(car2.brand);
//        System.out.println(car2.make);
//        System.out.println(car2.price);

        Car car3 = new Car();
//        car3.make = "Porsche";
//        car3.brand = "Spyder";
//        car3.price = 8000000;
//        System.out.println(car3.make);
//        System.out.println(car3.brand);
//        System.out.println(car3.price);

        car3.start();
        car2.start();
        car1.start();

        car1.setMake("Porsche");
        System.out.println(car1.getMake());
        car1.setBrand("Spyder");
        System.out.println(car1.getBrand());
        car1.setPrice(8000000);
        System.out.println(car1.getPrice());

        Car car4 = new Car();
        System.out.println(car4.getMake());
        System.out.println(car4.getBrand());
        System.out.println(car4.getPrice());

        Driver driver1 = new Driver("Marcuz", 21, "San Jose Del Monte City");
        System.out.println("Details of driver1:");
        System.out.println(driver1.getName());
        System.out.println(driver1.getAge());
        System.out.println(driver1.getAddress());

        Car car5 = new Car("Corolla", "Toyota",500000,driver1);
        System.out.println(car5.getMake());
        System.out.println(car5.getBrand());
        System.out.println(car5.getPrice());
        System.out.println(car5.getCarDriverName());

        Animal animal1 = new Animal("cookie", "black");
        animal1.call();







    }
}