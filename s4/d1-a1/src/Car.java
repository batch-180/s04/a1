public class Car {

    //An object is an idea of a real world object.
    //A class is the code that describes the object.
    //An instance is a tangible copy of an idea instantiated or created from a class.
    //Attributes and Methods
    private String make;
    private String brand;
    private int price;

    private Driver carDriver;

    //Methods are functions of an object which allows us to perform certain tasks
    //void - means that the function does not return anything. Because in Java, a function/methods' returns dataType must be declared.
    public void start() {
        System.out.println("Vroom! Vroom!");
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    //constructor is a method which allows us to set the initial values of an instance.
    //empty/default constructor - default constructor - allows is to create an instance with default initialized values of our properties.

    public Car(){
        //empty or you can designate default values instead of getting them from parameters.
        //Java, actually already creates one for us, however, empty/default constructor made just by Java allows us to Java to set its own default values.
    }

    public Car(String make, String brand, int price, Driver driver) {
        this.make = make;
        this.brand = brand;
        this.price = price;
        this.carDriver = driver;
    }

    public Driver getCarDriver() {
        return carDriver;
    }

    public void setCarDriver(Driver carDriver) {
        this.carDriver = carDriver;
    }
    public String getCarDriverName(){
        return  this.carDriver.getName();
    }
}

